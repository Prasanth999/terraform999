resource "aws_subnet" "private" {
    count             = length(slice(local.az_names, 0, 2))
    vpc_id            = aws_vpc.example.id
    cidr_block        = cidrsubnet(var.aws-vpc-cidr, 8, count.index + length(local.az_names) + 1)
    availability_zone = local.az_names[count.index]
    tags = {
        Name = "Private Subnet-${count.index + 1}"
    }
}

resource "aws_security_group" "nat-sg" {
  name        = "nat-sg"
  description = "Allow traffic to private subnets"
  vpc_id      = aws_vpc.example.id

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "Nat SG"
  }
}

resource "aws_route_table" "PrivateRoute" {
  vpc_id = aws_vpc.example.id

  route {
    cidr_block = "0.0.0.0/0"
    instance_id = aws_instance.nat.id
  }

  tags = {
    Name = "PrivateRoute"
  }
}

resource "aws_route_table_association" "private-sub-association" {
  count          = length(slice(local.az_names, 0, 2))
  subnet_id      = aws_subnet.private.*.id[count.index]
  route_table_id = aws_route_table.PrivateRoute.id
}