resource "aws_key_pair" "key" {
  key_name   = "key-pair-01"
  public_key = file("scripts/web.pub")
}