resource "aws_launch_configuration" "web_1c" {
  name                 = "web_1c"
  image_id             = var.ec2_ami_id[var.region]
  instance_type        = var.instance_type
  user_data            = file("scripts/apache.sh")
  security_groups      = [aws_security_group.ec2-sg.id] 
  key_name             = aws_key_pair.key.key_name
  iam_instance_profile = aws_iam_instance_profile.s3_ec2_profile.name
}

resource "aws_autoscaling_group" "web_asg" {
  name                 = "web-asg"
  max_size             = 2
  min_size             = 1
  vpc_zone_identifier  = local.pub_sub_ids
  health_check_grace_period = 60
  health_check_type    = "ELB"
  load_balancers       = [aws_elb.webserver_elb.name] 
  launch_configuration = aws_launch_configuration.web_1c.name
}

resource "aws_autoscaling_policy" "AddInstancesPolicy" {
  name                   = "AddInstancesPolicy"
  scaling_adjustment     = 1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 60
  autoscaling_group_name = aws_autoscaling_group.web_asg.name
}

resource "aws_autoscaling_policy" "RemoveInstancesPolicy" {
  name                   = "AddInstancesPolicy"
  scaling_adjustment     = -1
  adjustment_type        = "ChangeInCapacity"
  cooldown               = 60
  autoscaling_group_name = aws_autoscaling_group.web_asg.name
}

resource "aws_cloudwatch_metric_alarm" "avg_cpu_ge_80" {
  alarm_name          = "avg_cpu_ge_80"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "80"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web_asg.name
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.AddInstancesPolicy.arn]
}

resource "aws_cloudwatch_metric_alarm" "avg_cpu_le_30" {
  alarm_name          = "avg_cpu_le_80"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = "2"
  metric_name         = "CPUUtilization"
  namespace           = "AWS/EC2"
  period              = "120"
  statistic           = "Average"
  threshold           = "30"

  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.web_asg.name
  }

  alarm_description = "This metric monitors ec2 cpu utilization"
  alarm_actions     = [aws_autoscaling_policy.RemoveInstancesPolicy.arn]
}