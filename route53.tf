resource "aws_route53_zone" "prasanth999" {
    name     = "prasanth999.xyz"
  
}

resource "aws_route53_record" "www" {
    zone_id = aws_route53_zone.prasanth999.zone_id
    name = "www.prasanth999.xyz"
    type = "A"
    alias {
        name = aws_elb.webserver_elb.dns_name
        zone_id = aws_elb.webserver_elb.zone_id
        evaluate_target_health = true
    }
}

resource "aws_route53_record" "prasanth" {
    zone_id = aws_route53_zone.prasanth999.zone_id
    name = "prasanth999.xyz"
    type = "A"
    alias {
        name = aws_elb.webserver_elb.dns_name
        zone_id = aws_elb.webserver_elb.zone_id
        evaluate_target_health = true
    }
}

