provider "aws" {
    region = "ap-south-1"
}

module "dev_vpc" {
  source = "../modules/vpc"
  vpc_cidr = var.dev_vpc_cidr
  vpc_id = module.dev_vpc.vpc_id
  subnet_cidr = var.dev_subnet_cidr
}

module "dev_ec2" {
  source = "../modules/ec2"
  instance_type = "t2.micro"
  ami_id = "ami-0af25d0df86db00c1"
  ec2_count = 1
  subnet_id = module.dev_vpc.subnet_id
}