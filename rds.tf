resource "aws_db_instance" "mysql-instance" {
  identifier                 = "mysql-${terraform.workspace}"
  allocated_storage          = 20
  storage_type               = "gp2"
  engine                     = "mysql"
  engine_version             = "5.7"
  instance_class             = "db.t2.micro"
  publicly_accessible        = false 
  name                       = "admin"
  username                   = "admin"
  password                   = "Admin1234"
  parameter_group_name       = "default.mysql5.7"
  db_subnet_group_name       = aws_db_subnet_group.rds_mysql.id
  backup_window              = "01:00-01:30"
  auto_minor_version_upgrade = false
  vpc_security_group_ids     = [aws_security_group.rds_sg.id]
  skip_final_snapshot        = true
}

resource "aws_db_subnet_group" "rds_mysql" {
  name       = "rds-mysql"
  subnet_ids = aws_subnet.private.*.id
  tags = {
    Name     = "RDS Subnet Group"
  }
}

resource "aws_security_group" "rds_sg" {
  name = "rds_sg"
  description = "Allow traffic for rds"
  vpc_id = aws_vpc.example.id
  
  ingress {
    from_port        = 3306
    to_port          = 3306
    protocol         = "tcp"
   security_groups     = [aws_security_group.ec2-sg.id]
  }

  
  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
  }

  tags = {
    Name = "RDS-SG"
  }
}