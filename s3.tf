resource "aws_s3_bucket" "webserver_bucket" {
  bucket = var.bucket_name
  acl    = "private"
  
  tags = {
    Name        = "WEBSERVER BUCKET"
    Environment = "Dev"
  }
}