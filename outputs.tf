output "aws-vpc-cidr-block" {
  value = aws_vpc.example.cidr_block
}

output "aws-vpc-id-0" {
  value = aws_vpc.example.id
}

output "nat-public-ip" {
  value = aws_instance.nat.public_ip
}
