
resource "aws_instance" "nat" {
  ami           = var.nat_ami_id[var.region]
  instance_type = var.instance_type
  subnet_id     = local.pub_sub_ids[0]
  source_dest_check = false
  vpc_security_group_ids = [aws_security_group.nat-sg.id]
  tags = {
    Name = "Nat-Instance"
  }
}